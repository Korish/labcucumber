package com.ait.example.pages;

import com.ait.example.Show;
import com.ait.example.Member;

public class BuyTicketPage {

    private boolean buyTicketSuccess;

    private boolean onBuyTicketPage;

    public void open() {
        // simulate opening the buy ticket page...
        onBuyTicketPage = true;
    }

    public void buyTicket(Show game, Member member) {
        // simulate buying the ticket
    	System.out.println("The Show called  " + game.getName() + "and i have  " + member.getNumberOfTicketsBoughtLastSeason() );
        buyTicketSuccess = onBuyTicketPage && member.qualifiesFor(game);
    }

    public boolean isNotEnoughTicketsBoughtLastSeasonMessageShowing() {
        return !buyTicketSuccess;
    }

    public boolean isPaymentScreenDisplayed() {
        return buyTicketSuccess;
    }
}
