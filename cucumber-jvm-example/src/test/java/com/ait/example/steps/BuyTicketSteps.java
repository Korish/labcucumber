package com.ait.example.steps;

import com.ait.example.Show;
import com.ait.example.Member;
import com.ait.example.MemberRepository;
import com.ait.example.ShowRepository;
import com.ait.example.pages.BuyTicketPage;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BuyTicketSteps {

    @Autowired
    private ShowRepository showRepository;

    @Autowired
    private MemberRepository memberRepository;

    private BuyTicketPage buyTicketPage = new BuyTicketPage();

    @When("^I try to buy a ticket for the show called (.+)$")
    public void tryToBuyTicketFor(String name) {
        buyTicketPage.open();

        Show show = showRepository.findByName(name);
        Member member = memberRepository.findFirst();
        buyTicketPage.buyTicket(show, member);
    }

    @Then("^I should be able to buy the ticket$")
    public void assertThatIAmAbleToBuyTheTicket() {
        assertFalse(buyTicketPage.isNotEnoughTicketsBoughtLastSeasonMessageShowing());
        assertTrue(buyTicketPage.isPaymentScreenDisplayed());
    }

    @Then("^I should be told that I did not buy enough tickets last season$")
    public void shouldBeToldThatIDidNotBuyEnoughTicketsLastSeason() {
        assertTrue(buyTicketPage.isNotEnoughTicketsBoughtLastSeasonMessageShowing());
    }
}
